# DO NOT USE THIS

Use buildroot.org instead. This project has no added value.

I simply found buildroot too late, after I've spent day and a half writing this.

# Embedded Linux Helpers

Helper scripts for building small Linux distributions suitable for use in embedded devices.

Scripts in this repository are roughly based on books Linux From Scratch and Cross-Compiled Linux
From Scratch -- Embedded. However, Docker was added to the mix for easier dependency management,
easily reproducible builds using Continuous Integration, isolation from host system during
builds and checkpointable builds for easier development.

Build process consists of roughly the following steps:

  - **Prepare tools for cross-compiling**. Here, these tools are built in a docker image
    (`crosstools-image`). You can build this image locally for any architecture you want, or you can
    pull one of pre-built images from our registry (if it matches your needs).
  - **Target filesystem preparation**. Components are compiled and installed in place in a target
    filesystem. *TODO this is not yet present and under development*
  - **Target image creation**. Target filesystem is packaged to a single SD-card or upgrade image
    for distribution. *TODO this is not yet present and under development*

## `crosstools-image`

This is a Docker image based on Alpine Linux which contains required tools for cross-compiling.
Which architecture it is targeting is determined by the following build arguments:

  - CROSSCOMPILE_HOST: what host are we building on
  - CROSSCOMPILE_TARGET: target triplet (for example arm-linux-musleabihf)
  - CROSSCOMPILE_TGT_ARCH: target architecture (for example arm)
  - CROSSCOMPILE_TGT_ARM_ARCH: (if target architecture is ARM) target arm architecture
                               (for example armv7-a)
  - CROSSCOMPILE_TGT_FLOAT: type of FPU on the target (soft, softp, hard)
  - CROSSCOMPILE_TGT_FPU: hardware FPU version (for example vfpv4)

See http://clfs.org/view/clfs-embedded/arm/cross-tools/variables.html on how to choose these
variables. Use `--build-arg` parameter to `docker build` to set it. Additionally, consider adding
`--build-arg MAKEFLAGS="-j $(grep -c ^processor /proc/cpuinfo)"` for parallel builds.

Example build command:

```sh
docker build -t deadlock/crosstools:custom-armv7a-hard-vfpv4
  --build-arg MAKEFLAGS="-j $(grep -c ^processor /proc/cpuinfo)"
  --build-arg CROSSCOMPILE_TGT_FLOAT="hard"
  --build-arg CROSSCOMPILE_TGT_FPU="vfpv4"
  --build-arg CROSSCOMPILE_HOST="x86_64"
  --build-arg CROSSCOMPILE_TARGET="arm-linux-musleabihf"
  --build-arg CROSSCOMPILE_TGT_ARCH="arm"
  --build-arg CROSSCOMPILE_TGT_ARM_ARCH="armv7-a"
  crosstools-image
```

Since different combination of these variables produces a different image, be sure to tag it
appropriately.

Currently, image for `armv7a-hard-vfpv4` is automatically built on our CI, since that architecture
is the only one we need to support in Project Deadlock so far. If building these images for
different architecture would be helpful to you, feel free to open an issue.

### Modifying `crosstools-image`

Crosstools image utilizes Docker multi-stage builds
(https://docs.docker.com/develop/develop-images/multistage-build/). First, in image `alpine:3.7`
cross-compiler and other tools are compiled. Each required component is downloaded and built in a
separate `RUN` statement, which creates a new layer for each component. This is advantageous for
development, since if build of a component fails, or some component is modified, `docker build`
won't have to rebuild the whole image over. It will simply use previously built layers (if their
recipe, or recipe of one of their parent layers, was not modified). This can save huge amounts
of time when optimizing (or experimenting with) build parameter, since builds are reproducible,
always in a clean environment, and already built components are reusable.

After stage 1 completes cross-tools are built, but the image now has a huge number of layers and
contains a lot of unneeded components. Using it as-is would be impractical. That's why a second
stage begins, again with a clean `alpine:3.7` image, and it just installs host dependencies for
building linux distributions and copies previously compiled cross-compiling tools. Some environment
variables are set so that generic build scripts may later be used and that image is then used for
final builds.
